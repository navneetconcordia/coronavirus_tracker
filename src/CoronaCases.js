import React from 'react';
import {getAllLatestData} from '../src/APICall';
import Layout from './Body/Layout';

class CoronaCases extends React.Component {
    state = {
        data: undefined
    }

    componentDidMount() {
        this.getData('CA');
    }

    getData = (countryCode) => {
        this.setState({data: undefined});
        getAllLatestData(countryCode).then(data => {
            this.setState({data: data[0]})
        })
    }

    render() {
        const {data} = this.state;
        return (
            <Layout
                getData={this.getData}
                data={data}
            />
        )
    }
}

export default CoronaCases;