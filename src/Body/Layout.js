import React from "react";
import { Row, Col, Container, DropdownButton, Dropdown } from "react-bootstrap";
import "./Layout.css";
import * as image from "./index";
import countriesData from "../Countries.json";
import { Skeleton, Space } from "antd";

class Layout extends React.Component {
  state = {
    flag: image.CA_flag,
    country_name: "Canada",
  };

  changeFlag = (countryCode, countryName) => {
    this.setState({
      flag: image[countryCode + "_flag"],
      country_name: countryName,
    });
    this.props.getData(countryCode);
  };

  renderCountries = () => {
    return countriesData.map((nation) => {
      return (
        <Dropdown.Item
          eventKey={nation.countryCode}
          onSelect={() => this.changeFlag(nation.countryCode, nation.country)}
        >
          {nation.country}
        </Dropdown.Item>
      );
    });
  };

  renderSkeleton = () => {
    return (
      <Space>
        <Skeleton.Input style={{ width: 100 }} active={true} size={28} />
      </Space>
    );
  };

  render() {
    const { data } = this.props;
    return (
      <div>
        <Container fluid>
          <Row className="head">
            <Col>
              <h1
                style={{
                  marginTop: "1rem",
                  fontFamily: "Times New Roman",
                  fontWeight: "bolder",
                }}
              >
                Coronavirus Cases
              </h1>
            </Col>
          </Row>

            <Row className="middle">
              <Col className="small-image">
                <img src={this.state.flag} alt={"flag"} />
                <div className="real-data">
                  <h5>{this.state.country_name}</h5>
                  <div className="readings">
                    <h2>
                        <span style={{color: 'white'}}>
                      Total Cases:{" "}
                      </span>
                      {!data ? this.renderSkeleton() : data.totalConfirmed}
                    </h2>
                    <h2>
                    <span style={{color: 'white'}}>
                      Active Cases:{" "}
                      </span>
                      {!data ? this.renderSkeleton() : data.activeCases}
                    </h2>
                    <h2>
                    <span style={{color: 'white'}}>
                      New Infections:{" "}
                      </span>
                      {!data ? this.renderSkeleton() : data.dailyConfirmed}
                    </h2>
                    <h2>
                    <span style={{color: 'white'}}>
                      New Deaths:{" "}
                      </span>
                      {!data ? this.renderSkeleton() : data.dailyDeaths}
                    </h2>
                  </div>
                </div>
                <DropdownButton id="dropdown-basic-button" title="Country">
                  <div className="dropdown-style">{this.renderCountries()}</div>
                </DropdownButton>
              </Col>
            </Row>
          <Row className="foot">
            <Col></Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Layout;
