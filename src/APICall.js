const axios = require("axios");

export async function getAllLatestData(code) {
  let resp = await axios.get(
    `http://api.coronatracker.com/v3/stats/worldometer/country?countryCode=${code}`
  );
  return resp.data;
}